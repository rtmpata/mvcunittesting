﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVCUnitTesting.Models
{
    public class WorkingProductRepository : IRepository
    {
        public List<Product> GetAll()
        {

            return new List<Product>(){
                new Product { Genre = "Drama", ID=4, Name= "Palpasa",Price=10m},
                new Product { Genre = "Fiction", ID =3, Name="All Chemist", Price =10m },
                new Product { Genre="Fiction", ID=1, Name="Moby Dick", Price = 12.50m},
                new Product { Genre = "Fiction", ID = 2, Name = "War and Peace", Price = 17m }
            };
        }
    }
}