﻿using MVCUnitTesting.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVCUnitTesting.Controllers
{
    public class HomeController : Controller
    {
        private IRepository _repo;

        public HomeController()
        {
            this._repo = new WorkingProductRepository();
        }
        public HomeController(IRepository repo)
        {
            this._repo = repo;
        }
       


        public ViewResult FindByGenre(string genre)
        {
            var products = _repo.GetAll();
            List<Product> results = products.Where(b => b.Genre == genre).ToList();

            return View(results);
        }

        public ViewResult Index()
        {
            var products = _repo.GetAll();
            return View(products);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}